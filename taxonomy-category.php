<?php include 'header.php' ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="col-xs-12 col-sm-3">
	<div class="files__item">
		<span class="files__category"><?php the_category(', ') ?></span>
			<h3 class="files__title" title="Defense &amp; Security (Embraer)"><?php the_title();?></h3>
			<div class="files__thumb" title="Defense &amp; Security (Embraer)">            
				<div class="files__thumb__view">
					<?php the_post_thumbnail('full fix-thumb'); ?>
				</div>
			</div>
			<div class="files__download">
				<span class="span-down">Download</span>

				<div class="files__lang">       
					<?php if (get_field('pdf_portugues')) : ?>             
						<a title="Baixar arquivo: <?php the_title();?>" href="<?php the_field('pdf_portugues');?>" download="<?php the_field('pdf_portugues');?>">
							<span class="show-for-medium">Português</span>
						</a>  
					<?php endif; ?>                             
					<?php if (get_field('pdf_ingles')) : ?>
						<a title="Download file: <?php the_title();?>" href="<?php the_field('pdf_ingles');?>" download="<?php the_field('pdf_ingles');?>">
							<span class="show-for-medium">English</span>   
						</a>
					<?php endif; ?>                                   
				</div>
			</div>
		
	</div>
</div>

<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Nenhum Documento Encontrado.' ); ?></p>
<?php endif; ?>
<?php include 'footer.php' ?>