<?php include 'header.php' ?>
<div class="container view-post clearfix">
  <div class="row">

    <?php
      $cont = 1;
      if (have_posts()) : while (have_posts()) : the_post();
    ?>

    <div class="jumbo-heading">

      <h2 class="title-post"><?php the_title(); ?></h2>

    </div>

    <article id="" class="col-xs-12 item">
      <div class="post-entry">
        <div class="col-md-12 content-page">
          <div class="post-img">

            <?php the_post_thumbnail(); ?>

          </div>
          <!-- Blog Post Start -->

          <div class="col-md-12 blog-post">
            

            <p class="text-post"><?php the_content(); ?></p>

          </div>

        </div>
      </div>

  </article>
  <?php
        $cont ++;
      endwhile;
    endif;
    ?>
  </div>
</div>

<?php include 'footer.php'?>
