<?php

/**************************************
 *  THEME SUPORT
 **************************************/
function add_suport_theme(){
    add_theme_support( 'post-thumbnails' );
}
add_action('after_setup_theme','add_suport_theme');

/**************************************
 * Registro Menu Personalizado
 **************************************/
add_theme_support('menus');
register_nav_menus( array(
    'primary' => __( 'Menu header', 'menu-header' ),
) );

/**************************************
 *  SCRIPTS / CSS
 **************************************/
function wp_responsivo_scripts() {
  // Carregando CSS header
  wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
  wp_enqueue_style( 'style', get_stylesheet_uri() );

  // Carregando Scripts header
  wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/assets/js/bootstrap.min.js', array('jquery') );

  //Carregando no footer
  //wp_enqueue_script('functions-js', get_template_directory_uri().'/assets/js/functions.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'wp_responsivo_scripts' );


/**************************************
 * Registro Custom Post type Documentos
 **************************************/
add_action('init', 'documentos_registrer');
function documentos_registrer(){
     $labels = array(
        'name' => _x('Documentos', 'post type general name'),
        'singular_name' => _x('Documentos', 'post type singular name'),
        'add_new' => _x('Adicionar Documento', 'Documento'),
        'add_new_item' => __('Adicionar Documento'),
        'edit_item' => __('Editar Documento'),
        'new_item' => __('Novo Documento'),
        'view_item' => __('Ver Documento'),
        'search_items' => __('Procurar Documento'),
        'not_found' =>  __('Nada encontrado'),
        'not_found_in_trash' => __('Nada encontrado no lixo'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-pressthis',
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug'=>'documentos'),
        'menu_position' => 6,
        'supports' => array('title','editor','thumbnail'),
        'taxonomies'          => array( 'category' ),
      );
    register_post_type('documentos',$args);
}

/**************************************
 * Read More
 **************************************/
function more_post_ajax(){
    $offset = $_POST["offset"];
    $ppp = $_POST["ppp"];
    header("Content-Type: text/html");

    $args = array(
        'post_type' => 'documentos',
        'posts_per_page' => $ppp,
        'offset' => $offset,
        'order' => 'ASC',
    );

    $loop = new WP_Query($args);
    while ($loop->have_posts()) { $loop->the_post(); 
       the_content();
    }

    exit; 
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax'); 
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');
