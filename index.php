<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6 demo-2 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7 demo-2 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8 demo-2 no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="pt-br">
<!--<![endif]-->
<?php 

    global $wp_query;
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Teste Embraer WS</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Metas SEO -->
	<meta property="og:url"                content="" />
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="" />
	
	<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" media="screen">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/39668247fc.js" crossorigin="anonymous"></script>
</head>

<body>
    <header class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a href="https://embraerdefense.digitalws.co/">
                        <img width="170" alt="Embraer" src="<?php bloginfo( 'template_directory' );?>/img/logo-embraer.png">
                    </a>
                </div>
            </div>
        </div>
    </header>
    <section class="title-box text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Embraer Defense &amp; Security Brochures</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="navegation text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="filter">
                        <div class="filter__icon hide-for-medium">
                            <span class="cat">All</span>
                        </div>
                        <div class="filter__link">
                            <a class="active" href="#">All</a>
                            <?php 
                                $categories = get_terms( array(
                                    'taxonomy' => 'category',
                                    'hide_empty' => true,
                                ) );
                                foreach ($categories as $category){
                            
                                echo '<a class="' . $category->slug . '"  title="' . $category->name . '" href="' . get_category_link($category->term_id) . '">' . "$category->slug" . '</a>';
                            
                              }  
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <main>
        <div class="container">
            
            <div class="row">
                <?php 

                    $postsPerPage = 4;
                    $args = array(
                        'post_type' => 'documentos',
                        'post_status' => 'publish',
                        'posts_per_page' => $postsPerPage,
                        'order' => 'ASC',
                    );

                    $loop = new WP_Query($args);
                    while ($loop->have_posts()) : $loop->the_post();

                ?>
                
                <?php get_template_part( 'content' );?>                
                <?php
                    endwhile; 
                    echo '
                        <div class="fetch text-center"> 

                            <a id="more_posts" href="#" class="button custom padding">Load More</a>                                      

                        </div>
                    ';
                    wp_reset_postdata(); 
                ?>          
            </div>


        </div>
    </main>
    
    <footer class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 cell">                                
                    <div>
                        <a href="https://embraerdefense.digitalws.co/">
                            <img width="170" alt="Embraer" src="<?php bloginfo( 'template_directory' );?>/img/logo-embraer-footer.png">
                        </a>
                    </div>
                    <div class="social">
                        <a href="#" title="Embraer no Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" title="Embraer no Instagram" target="_blank"><i class="fab fa-instagram"></i></i></a>
                        <a href="#" title="Embraer no Linkedin" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        <a href="#" title="Embraer no Twitter" target="_blank"><i class="fab fa-twitter"></i></a>
                        <a href="#" title="Embraer no Youtube" target="_blank"><i class="fab fa-youtube"></i></a>
                    </div>

                </div>
            </div>
        </div>
    </footer>

    <a id="go" title="Go to top"><i class="fas fa-arrow-up"></i></a>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript">   
       
    </script>

    <script type="text/javascript">
        $('.filter').click(function(){
            $('.filter__link').toggleClass("active");
        });

        $("#go").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
        });
    </script>

    <script>
        //Get the button
        var mybutton = document.getElementById("go");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }
    </script>

</body>
</html>
