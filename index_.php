<?php include 'header.php' ?>
<section class="slider clearfix">
   <div class="">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
         <!-- Indicators -->
         <ol class="carousel-indicators">
            <?php
               $args = array('post_type'=>'slider', 'showposts'=>5);
               $my_slider = get_posts( $args );
               $count = 0 ; if($my_slider) : foreach($my_slider as $post) : setup_postdata( $post );
            ?>
            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $count; ?>" <?php if($count == 0): ?> class="active"<?php endif; ?>></li>
            <?php
               $count ++ ;
               endforeach;
               endif;
            ?>
         </ol>

      <!-- Wrapper for slides -->
         <div class="carousel-inner" role="listbox">
            <?php
               $cont = 0 ; if($my_slider) : foreach($my_slider as $post) : setup_postdata( $post );
            ?>
               <div class="item <?php if($cont == 0) echo "active"; ?>">
                  <?php the_post_thumbnail('full'); ?>
                  <div class="container">
                     <div class="row">
                        <div class="carousel-caption col-12">
                           <h2><?php the_title(); ?></h2>
                           <p><?php the_content(); ?></p>
                           <a class="leia-mais" href="#">LEIA MAIS</a>
                        </div>
                     </div>
                  </div>
               </div>
            <?php
               $cont ++ ;
               endforeach;
               endif;
            ?>
         </div>

      </div>
   </div>
</section>


<main>
      <section class="novidades">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <h2 class="titles">NOVIDADES</h2>
               </div>
            </div>
         </div>

         <!-- Destaques -->
         <div class="container">
            <div class="row list-post">
                             
                  <?php
                     $args = array(
                        'cat' => 4, 
                        'posts_per_page' => 3,
                     );

                     $post_loop = new WP_Query( $args );

                     if ( $post_loop->have_posts() ) : while ( $post_loop->have_posts() ) : $post_loop->the_post();	
                  ?>
                     <div class="col-xs-12 col-sm-4">
                        <div class="item-post">
                           <div class="img-content">
                              <?php the_post_thumbnail('full'); ?>
                           </div>

                           <div class="text-content">
                              <h3 class="categoria"><?php the_field('categoria');?></h3>
                              <h3><?php the_title();?></h3>
                              <div class="author-content">
                                 <h4>POR<?php the_author_posts_link(); ?></h4>
                                 <h4 class="date"><?php the_date('d M');?></h4>
                              </div>
                              <p><?php the_excerpt();?></p>
                           </div>
                        </div>
                     </div>   
                  <?php
                        endwhile;
                     endif;
                  ?>
               
            </div>      
         </div>
         <!-- Fim Destaques -->
         <!-- Banner Destaque -->
         <section class="banner-destaque">
            <div class="container">
               <div class="row">
                  <?php
                     $args = array('post_type'=>'banner', 'showposts'=>1);
                     $my_banner = get_posts( $args );
                     if($my_banner) : foreach($my_banner as $post) : setup_postdata( $post );
                  ?>
                  <div class="col-xs-12">
                     <div class="img-banner">
                        <?php the_post_thumbnail('full'); ?>
                     </div>
                  </div>
                  <?php 
                        endforeach;
                     endif;
                  ?>
               </div>
            </div>
         </section>
         <!-- Fim Banner Destaque -->
         <!-- Listagem Geral do Post -->
         <section class="list-post">
            <div class="container">
               <div class="row">

               <?php
                  $args = array(
                     'post_type' => 'post',
                     'post_status' => 'publish',
                     'posts_per_page' => '6',
                     'paged' => 1,
                  
                  );
                  $post_loop = new WP_Query( $args );

                  if ( $post_loop->have_posts() ) : while ( $post_loop->have_posts() ) : $post_loop->the_post();	
               ?>
                  <div class="col-xs-12 col-sm-4">
                     <div class="item-post">
                        <div class="img-content">
                           <?php the_post_thumbnail('full'); ?>
                        </div>

                        <div class="text-content">
                           <h3 class="categoria"><?php the_field('categoria');?></h3>
                           <h3><?php the_title();?></h3>
                           <div class="author-content">
                              <h4>POR<?php the_author_posts_link(); ?></h4>
                              <h4 class="date"><?php the_date('d M');?></h4>
                           </div>
                           <p><?php the_excerpt();?></p>
                        </div>
                     </div>
                  </div>
               <?php
                     endwhile;
                  endif;
               ?>

               </div>
            </div>
         </section>
         <!-- Listagem Geral do Post -->
         <!-- Mais Lidos -->
         <section class="mais-lidos">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <h2 class="titles">MAIS LIDOS</h2>
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="row list-post">

               <?php
                  $args = array(
                     'post_type' => 'post',
                     'post_status' => 'publish',
                     'posts_per_page' => '6',
                     'paged' => 1,
                  
                  );
                  $post_loop = new WP_Query( $args );

                  if ( $post_loop->have_posts() ) : while ( $post_loop->have_posts() ) : $post_loop->the_post();	
               ?>
                  <div class="col-xs-12 col-sm-4">
                     <div class="item-post">
                        <div class="img-content">
                           <?php the_post_thumbnail('full'); ?>
                        </div>

                        <div class="text-content">
                           <h3 class="categoria"><?php the_field('categoria');?></h3>
                           <h3><?php the_title();?></h3>
                           <div class="author-content">
                              <h4>POR<?php the_author_posts_link(); ?></h4>
                              <h4 class="date"><?php the_date('d M');?></h4>
                           </div>
                           <p><?php the_excerpt();?></p>
                        </div>
                     </div>
                  </div>   
               <?php
                     endwhile;
                  endif;
               ?>

               </div>
            </div>
         </section>
         <!--Mais lidos-->
         <!-- NewsLetter -->
         <section class="newsletter">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <h2 class="titles">NEWSLETTER</h2>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12">
                     <div class="formNews">
                           <div class="textNews">
                              <h3>Fique por dentro das nossas novidades por e-mail.</h3>
                           </div>
                                          
                           <form class="form-inline">
                              <input class="" type="search" placeholder="Digite Seu E-mail" aria-label="Search">
                              <button class="" type="submit">ENVIAR</button>
                           </form>
                  
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- NewsLetter -->
         <!-- Descrição Blog -->
         <section class="descricao">
            <div class="container">
               <div class="row">
                  <?php
                     $args = array('post_type'=>'descricao', 'showposts'=>1);
                     $my_desc = get_posts( $args );
                     if($my_desc) : foreach($my_desc as $post) : setup_postdata( $post );
                  ?>
                  <div class="col-xs-12 col-sm-4">
                     <h3><?php the_title();?></h3>
                  </div>
                  <div class="col-xs-12 col-sm-8">
                     <p><?php the_field('descricao');?></p>
                  </div>
                  <?php 
                        endforeach;
                     endif;
                  ?>
               </div>
            </div>
         </section>
         <!-- Descrição Blog -->
      </section>
</main>
<?php include 'footer.php' ?>